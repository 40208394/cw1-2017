﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Invoice : Form
    {
        public Invoice()
        {
            InitializeComponent();


        }
        public string invoiceFname
        {
            get
            {
                return this.invFname.Text;
            }
            set
            {
                this.invFname.Text = value;
            }
        }
        public string invoiceCourse
        {
            get
            {
                return this.invCourse.Text;
            }
            set
            {
                this.invCourse.Text = value;
            }
        }
        public string invoiceCost
        {
            get
            {
                return this.invCost.Text;
            }
            set
            {
                this.invCost.Text = value;
            }
        }

        private void Invoice_Load(object sender, EventArgs e)
        {

        }
    }
}

