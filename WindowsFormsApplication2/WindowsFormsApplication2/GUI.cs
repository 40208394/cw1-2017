﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class GUI : Form
    {
        public GUI()
        {
            InitializeComponent();
        }
       Conference a;
        private void button1_Click(object sender, EventArgs e)
       {
           try
           {
               a = new Conference();
               a.fName = name1.Text;
               a.sName = name2.Text;
               a.aref = Convert.ToInt32(aref1.Text);
               a.confName = conf1.Text;
               a.regType = regType1.Text;
               a.paid = paid1.Checked;
               a.presenter = presenter1.Checked;
               a.instName = inst1.Text;
               a.paperTitle = paper1.Text;
           }

           catch (Exception excep)
           {
               MessageBox.Show(excep.Message);
           }
            
        }

        private void clear_Click(object sender, EventArgs e)
        {
            name1.Text = "";
            name2.Text = "";
            aref1.Text = "";
            inst1.Text = "";
            conf1.Text = "";
            regType1.Text = "";
            paid1.Checked = false;
            presenter1.Checked = false;
            paper1.Text = "";
        }

        private void get_Click(object sender, EventArgs e)
        {
            name1.Text = a.fName;
            name2.Text=a.sName;
            aref1.Text=Convert.ToString(a.aref);
            inst1.Text=a.instName;
            conf1.Text=a.confName;
            regType1.Text=a.regType;
            paid1.Checked= a.paid;
            presenter1.Checked = a.presenter;
            paper1.Text = a.paperTitle;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void invoice_Click(object sender, EventArgs e)
        {
            if (paid1.Checked == false)
            {
                Invoice b = new Invoice();
                b.invoiceFname = a.fName;
                b.invoiceCourse = a.confName;
                b.invoiceCost =Convert.ToString( a.getCost(a.regType));
                b.Show();
            }
            else
            {
                MessageBox.Show(a.fName + " " + a.sName + " has paid the " + a.getCost(a.regType) + " pounds for his attendance to the " + a.confName + " conference");
            }
        }

        private void certificate_Click(object sender, EventArgs e)
        {
            Certificate c = new Certificate();
            if (presenter1.Checked)
            {
                c.certificate = "This is to certify that " + a.fName + " " + a.sName + " attended " + a.confName + " and presentet a paper entitled " + a.paperTitle;
            }
            else
            {
                c.certificate = "This is to certify that " + a.fName + " " + a.sName + " attended " + a.confName;
                
            }
            c.Show();
        }

        private void GUI_Load(object sender, EventArgs e)
        {

        }
    }
}
