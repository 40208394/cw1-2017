﻿namespace WindowsFormsApplication2
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameL = new System.Windows.Forms.Label();
            this.aRef = new System.Windows.Forms.Label();
            this.name1 = new System.Windows.Forms.TextBox();
            this.set = new System.Windows.Forms.Button();
            this.aref1 = new System.Windows.Forms.TextBox();
            this.instName = new System.Windows.Forms.Label();
            this.inst1 = new System.Windows.Forms.TextBox();
            this.confName = new System.Windows.Forms.Label();
            this.conf1 = new System.Windows.Forms.TextBox();
            this.regName = new System.Windows.Forms.Label();
            this.paid = new System.Windows.Forms.Label();
            this.presenter = new System.Windows.Forms.Label();
            this.paperTitle = new System.Windows.Forms.Label();
            this.paper1 = new System.Windows.Forms.TextBox();
            this.name2 = new System.Windows.Forms.TextBox();
            this.clear = new System.Windows.Forms.Button();
            this.get = new System.Windows.Forms.Button();
            this.invoice = new System.Windows.Forms.Button();
            this.certificate = new System.Windows.Forms.Button();
            this.paid1 = new System.Windows.Forms.CheckBox();
            this.presenter1 = new System.Windows.Forms.CheckBox();
            this.regType1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // nameL
            // 
            this.nameL.AutoSize = true;
            this.nameL.Location = new System.Drawing.Point(48, 40);
            this.nameL.Name = "nameL";
            this.nameL.Size = new System.Drawing.Size(35, 13);
            this.nameL.TabIndex = 0;
            this.nameL.Text = "Name";
            // 
            // aRef
            // 
            this.aRef.AutoSize = true;
            this.aRef.Location = new System.Drawing.Point(48, 79);
            this.aRef.Name = "aRef";
            this.aRef.Size = new System.Drawing.Size(70, 13);
            this.aRef.TabIndex = 1;
            this.aRef.Text = "Attendee Ref";
            // 
            // name1
            // 
            this.name1.Location = new System.Drawing.Point(153, 37);
            this.name1.Name = "name1";
            this.name1.Size = new System.Drawing.Size(100, 20);
            this.name1.TabIndex = 2;
            // 
            // set
            // 
            this.set.Location = new System.Drawing.Point(51, 347);
            this.set.Name = "set";
            this.set.Size = new System.Drawing.Size(75, 23);
            this.set.TabIndex = 3;
            this.set.Text = "SET";
            this.set.UseVisualStyleBackColor = true;
            this.set.Click += new System.EventHandler(this.button1_Click);
            // 
            // aref1
            // 
            this.aref1.Location = new System.Drawing.Point(153, 76);
            this.aref1.Name = "aref1";
            this.aref1.Size = new System.Drawing.Size(100, 20);
            this.aref1.TabIndex = 4;
            // 
            // instName
            // 
            this.instName.AutoSize = true;
            this.instName.Location = new System.Drawing.Point(48, 120);
            this.instName.Name = "instName";
            this.instName.Size = new System.Drawing.Size(83, 13);
            this.instName.TabIndex = 5;
            this.instName.Text = "Institution Name";
            // 
            // inst1
            // 
            this.inst1.Location = new System.Drawing.Point(153, 117);
            this.inst1.Name = "inst1";
            this.inst1.Size = new System.Drawing.Size(100, 20);
            this.inst1.TabIndex = 6;
            // 
            // confName
            // 
            this.confName.AutoSize = true;
            this.confName.Location = new System.Drawing.Point(48, 159);
            this.confName.Name = "confName";
            this.confName.Size = new System.Drawing.Size(93, 13);
            this.confName.TabIndex = 7;
            this.confName.Text = "Conference Name";
            // 
            // conf1
            // 
            this.conf1.Location = new System.Drawing.Point(153, 156);
            this.conf1.Name = "conf1";
            this.conf1.Size = new System.Drawing.Size(100, 20);
            this.conf1.TabIndex = 8;
            // 
            // regName
            // 
            this.regName.AutoSize = true;
            this.regName.Location = new System.Drawing.Point(48, 199);
            this.regName.Name = "regName";
            this.regName.Size = new System.Drawing.Size(90, 13);
            this.regName.TabIndex = 9;
            this.regName.Text = "Registration Type";
            // 
            // paid
            // 
            this.paid.AutoSize = true;
            this.paid.Location = new System.Drawing.Point(48, 242);
            this.paid.Name = "paid";
            this.paid.Size = new System.Drawing.Size(28, 13);
            this.paid.TabIndex = 11;
            this.paid.Text = "Paid";
            // 
            // presenter
            // 
            this.presenter.AutoSize = true;
            this.presenter.Location = new System.Drawing.Point(48, 281);
            this.presenter.Name = "presenter";
            this.presenter.Size = new System.Drawing.Size(52, 13);
            this.presenter.TabIndex = 13;
            this.presenter.Text = "Presenter";
            // 
            // paperTitle
            // 
            this.paperTitle.AutoSize = true;
            this.paperTitle.Location = new System.Drawing.Point(48, 315);
            this.paperTitle.Name = "paperTitle";
            this.paperTitle.Size = new System.Drawing.Size(58, 13);
            this.paperTitle.TabIndex = 15;
            this.paperTitle.Text = "Paper Title";
            // 
            // paper1
            // 
            this.paper1.Location = new System.Drawing.Point(153, 312);
            this.paper1.Name = "paper1";
            this.paper1.Size = new System.Drawing.Size(174, 20);
            this.paper1.TabIndex = 16;
            // 
            // name2
            // 
            this.name2.Location = new System.Drawing.Point(283, 37);
            this.name2.Name = "name2";
            this.name2.Size = new System.Drawing.Size(100, 20);
            this.name2.TabIndex = 17;
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(153, 347);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(75, 23);
            this.clear.TabIndex = 18;
            this.clear.Text = "CLEAR";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // get
            // 
            this.get.Location = new System.Drawing.Point(263, 347);
            this.get.Name = "get";
            this.get.Size = new System.Drawing.Size(75, 23);
            this.get.TabIndex = 19;
            this.get.Text = "GET";
            this.get.UseVisualStyleBackColor = true;
            this.get.Click += new System.EventHandler(this.get_Click);
            // 
            // invoice
            // 
            this.invoice.Location = new System.Drawing.Point(361, 347);
            this.invoice.Name = "invoice";
            this.invoice.Size = new System.Drawing.Size(75, 23);
            this.invoice.TabIndex = 20;
            this.invoice.Text = "INVOICE";
            this.invoice.UseVisualStyleBackColor = true;
            this.invoice.Click += new System.EventHandler(this.invoice_Click);
            // 
            // certificate
            // 
            this.certificate.Location = new System.Drawing.Point(199, 376);
            this.certificate.Name = "certificate";
            this.certificate.Size = new System.Drawing.Size(104, 23);
            this.certificate.TabIndex = 21;
            this.certificate.Text = "CERTIFICATE";
            this.certificate.UseVisualStyleBackColor = true;
            this.certificate.Click += new System.EventHandler(this.certificate_Click);
            // 
            // paid1
            // 
            this.paid1.AutoSize = true;
            this.paid1.Location = new System.Drawing.Point(148, 241);
            this.paid1.Name = "paid1";
            this.paid1.Size = new System.Drawing.Size(69, 17);
            this.paid1.TabIndex = 22;
            this.paid1.Text = "Has Paid";
            this.paid1.UseVisualStyleBackColor = true;
            // 
            // presenter1
            // 
            this.presenter1.AutoSize = true;
            this.presenter1.Location = new System.Drawing.Point(148, 280);
            this.presenter1.Name = "presenter1";
            this.presenter1.Size = new System.Drawing.Size(91, 17);
            this.presenter1.TabIndex = 23;
            this.presenter1.Text = "Is a Presenter";
            this.presenter1.UseVisualStyleBackColor = true;
            // 
            // regType1
            // 
            this.regType1.FormattingEnabled = true;
            this.regType1.Items.AddRange(new object[] {
            "Student",
            "Full",
            "Organizer"});
            this.regType1.Location = new System.Drawing.Point(153, 196);
            this.regType1.Name = "regType1";
            this.regType1.Size = new System.Drawing.Size(121, 21);
            this.regType1.TabIndex = 24;
            this.regType1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 405);
            this.Controls.Add(this.regType1);
            this.Controls.Add(this.presenter1);
            this.Controls.Add(this.paid1);
            this.Controls.Add(this.certificate);
            this.Controls.Add(this.invoice);
            this.Controls.Add(this.get);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.name2);
            this.Controls.Add(this.paper1);
            this.Controls.Add(this.paperTitle);
            this.Controls.Add(this.presenter);
            this.Controls.Add(this.paid);
            this.Controls.Add(this.regName);
            this.Controls.Add(this.conf1);
            this.Controls.Add(this.confName);
            this.Controls.Add(this.inst1);
            this.Controls.Add(this.instName);
            this.Controls.Add(this.aref1);
            this.Controls.Add(this.set);
            this.Controls.Add(this.name1);
            this.Controls.Add(this.aRef);
            this.Controls.Add(this.nameL);
            this.Name = "GUI";
            this.Text = "Conference";
            this.Load += new System.EventHandler(this.GUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameL;
        private System.Windows.Forms.Label aRef;
        private System.Windows.Forms.TextBox name1;
        private System.Windows.Forms.Button set;
        private System.Windows.Forms.TextBox aref1;
        private System.Windows.Forms.Label instName;
        private System.Windows.Forms.TextBox inst1;
        private System.Windows.Forms.Label confName;
        private System.Windows.Forms.TextBox conf1;
        private System.Windows.Forms.Label regName;
        private System.Windows.Forms.Label paid;
        private System.Windows.Forms.Label presenter;
        private System.Windows.Forms.Label paperTitle;
        private System.Windows.Forms.TextBox paper1;
        private System.Windows.Forms.TextBox name2;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button get;
        private System.Windows.Forms.Button invoice;
        private System.Windows.Forms.Button certificate;
        private System.Windows.Forms.CheckBox paid1;
        private System.Windows.Forms.CheckBox presenter1;
        private System.Windows.Forms.ComboBox regType1;
    }
}

