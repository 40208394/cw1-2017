﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    class Conference
    {
        private int _aref;

        public int aref
        {
            get
            {
                return _aref;
            }
            set
            {
                if (value<40000 || value>60000)
                {
                    throw new ArgumentException("Wrong Size!");
                }
                _aref = value;
            }
        }
        private string _fName;
        public string fName
        {
            get
            {
                return _fName;

            }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Blank Field!");
                }
                _fName = value;

            }
        }
        
        private string _sName;
        public string sName
        {
            get
            {
                return _sName;

            }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Blank Field!");
                }
                _sName = value;

            }
        }
        
        private string _instName;
        public string instName
        {
            get
            {
                return _instName;

            }
            set
            {
                
                _instName = value;

            }
        }
        
        private string _confName;
        public string confName
        {
            get
            {
                return _confName;

            }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Blank Field!");
                }
                _confName = value;

            }
        }
        
        public string _regType;
        public string regType
        {
            get
            {
                return _regType;

            }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Blank Field!");
                }
                _regType = value;

            }
        }
        
        private bool _paid;
        public bool paid
        {
            get
            { 
                return _paid; 
            }
            set
            {
                _paid = value; 
            }
        }
        
        private bool _presenter;
        public bool presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                _presenter = value;
            }
        }
        private string _paperTitle;

        public string paperTitle
        {
            get
            {
                return _paperTitle;
            }
            set
            {
                if (presenter==true && value == "")
                {
                    throw new ArgumentException("Presenter Needs a Paper to Present!");
                }
                _paperTitle = value;
            }
        }

       public int getCost(string x)
    {
        int cost=0;
        if(x=="Student")
        {
            cost = 300;
        }
        if (x == "Full")
        {
             cost = 500;
        }
        if (x == "Organizer")
        {
             cost = 0;
        }
        if (presenter == true) { return cost = cost - cost / 10; }
        else { return cost; }
    } 
    }

}
