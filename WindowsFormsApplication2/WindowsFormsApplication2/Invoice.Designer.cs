﻿namespace WindowsFormsApplication2
{
    partial class Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.invName = new System.Windows.Forms.Label();
            this.invFname = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.invCourse = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.invCost = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // invName
            // 
            this.invName.AutoSize = true;
            this.invName.Location = new System.Drawing.Point(41, 42);
            this.invName.Name = "invName";
            this.invName.Size = new System.Drawing.Size(35, 13);
            this.invName.TabIndex = 0;
            this.invName.Text = "Name";
            // 
            // invFname
            // 
            this.invFname.AutoSize = true;
            this.invFname.Location = new System.Drawing.Point(141, 42);
            this.invFname.Name = "invFname";
            this.invFname.Size = new System.Drawing.Size(101, 13);
            this.invFname.TabIndex = 1;
            this.invFname.Text = "Name of the Person";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name of course";
            // 
            // invCourse
            // 
            this.invCourse.AutoSize = true;
            this.invCourse.Location = new System.Drawing.Point(141, 93);
            this.invCourse.Name = "invCourse";
            this.invCourse.Size = new System.Drawing.Size(68, 13);
            this.invCourse.TabIndex = 3;
            this.invCourse.Text = "course name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Amount due";
            // 
            // invCost
            // 
            this.invCost.AutoSize = true;
            this.invCost.Location = new System.Drawing.Point(141, 146);
            this.invCost.Name = "invCost";
            this.invCost.Size = new System.Drawing.Size(28, 13);
            this.invCost.TabIndex = 5;
            this.invCost.Text = "Cost";
            // 
            // Invoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 278);
            this.Controls.Add(this.invCost);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.invCourse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.invFname);
            this.Controls.Add(this.invName);
            this.Name = "Invoice";
            this.Text = "Invoice";
            this.Load += new System.EventHandler(this.Invoice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label invName;
        private System.Windows.Forms.Label invFname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label invCourse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label invCost;
    }
}